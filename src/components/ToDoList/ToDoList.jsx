import ToDoCard from "../ToDoCard/ToDoCard";

function ToDoList(props) {
  return <>{props.arrayOfToDoItems.map(todo => (<ToDoCard deleteToDoCard={props.deleteToDoCard} key={todo.id} todo={todo}/>))}</>;
}

export default ToDoList;
