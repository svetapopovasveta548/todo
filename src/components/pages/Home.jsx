import ToDoInput from "../ToDoInput/ToDoInput";
import React from "react";
import { useState } from "react";
import ToDoList from "../ToDoList/ToDoList";

function Home() {
  const [arrayOfToDoItems, setArrayOfToDoItems] = useState([]);

  const getInputValue = (ToDoValue) => {
    setArrayOfToDoItems((prev) => [...prev, ToDoValue]);
  };

  const deleteToDoCard = (cardId) => {
    const filterArray = arrayOfToDoItems.filter(card => card.id !== cardId)
    setArrayOfToDoItems(filterArray)
  };

  return (
    <React.Fragment>
      <ToDoInput getInputValue={getInputValue} />
      <ToDoList
        deleteToDoCard={deleteToDoCard}
        arrayOfToDoItems={arrayOfToDoItems}
      />
    </React.Fragment>
  );
}

export default Home;
